# sky-test-react

For this example app, no dependencies have been added. For a larger app, redux and router would have been applied.

Install:

```
yarn
```

Start:

```
yarn start
```
