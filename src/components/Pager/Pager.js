import React from "react";

const Pager = ({ pageLimit, pagerClick, totalResults }) => {
  const numPages = totalResults / pageLimit;
  const links = [];

  for (let i = 1; i <= numPages; i++) {
    links.push(<button onClick={() => pagerClick(i)}>{i}</button>);
  }

  return <div>{links}</div>;
};

export default Pager;
