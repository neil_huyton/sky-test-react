import React from "react";

import Pager from "./components/Pager/Pager";

class App extends React.Component {
  state = {
    page: 1,
    originalResults: [],
    results: [],
    query: ""
  };

  componentDidMount() {
    window.onpopstate = () => {
      const query = this.getCurrentPath();
      this.setState({ query });
      this.getData(1, query);
    };
  }

  getCurrentPath = () => {
    const path = document.location.pathname;
    return path.substring(path.lastIndexOf("/"));
  };

  getData = (page, query) => {
    fetch(`https://help-search-api-prod.herokuapp.com/search?query=${query}`)
      .then(res => res.json())
      .then(data => {
        const originalResults = data.results;
        const results = originalResults.slice(page - 1, 10);
        this.setState({ originalResults, results });
      })
      .catch(console.log);
  };

  handleChange = e => {
    const query = e.target.value;
    this.setState({ query });
  };

  handleClick = e => {
    e.preventDefault();
    const { page, query } = this.state;
    // eslint-disable-next-line no-restricted-globals
    history.pushState(null, "", `${query}`);
    this.getData(page, query);
  };

  pagerClick = page => {
    const { originalResults } = this.state;
    const start = (page - 1) * 10;
    const end = start + 10;
    const results = originalResults.slice(start, end);

    this.setState({ results });
  };

  render() {
    const { originalResults, results } = this.state;
    return (
      <main>
        <form>
          <input onChange={e => this.handleChange(e)} />
          <button onClick={e => this.handleClick(e)}>Submit</button>
        </form>
        {results.length ? (
          results.map(item => (
            <div key={item.title}>
              <h3>
                <a href={item.url} target="_blank" rel="noopener noreferrer">
                  {item.title}
                </a>
              </h3>
              <p>{item.description}</p>
            </div>
          ))
        ) : (
          <p>No results</p>
        )}
        <Pager
          totalResults={originalResults.length}
          pageLimit={10}
          pagerClick={this.pagerClick}
        />
      </main>
    );
  }
}

export default App;
